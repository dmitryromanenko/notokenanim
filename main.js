'use strict';

Hooks.on('ready', () => {
	game.settings.register('notokenanim', 'enabled', {
		name: 'Disable Token Movement Animations',
		default: false,
		type: Boolean,
		scope: 'client',
		config: true,
		hint: 'Dragging and dropping tokens around the map will resolve instantly instead of sliding.'
	});
});

CanvasAnimation.animateLinear = (function () {
	const cached = CanvasAnimation.animateLinear;
	return function (attributes, options = {}) {
		if (game.settings.get('notokenanim', 'enabled')
			&& /Token\.[^.]+\.animateMovement/.test(options.name))
		{
			options.duration = 0;
		}

		return cached.apply(this, arguments);
	};
})();
